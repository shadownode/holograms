# Holograms Sponge

This project is a basic holograms plugin for [SpongeForge](https://github.com/SpongePowered/SpongeForge) which uses invisible armor stands to display floating messages.

* [Source]
* [Issues]
* [Website]
* [Downloads]

Licence: [MIT](LICENSE.md)

For instructions on how to use and or configure Holograms check out the basic configuration created at startup.

## Contributions

Would you like to contribute some code? Do you have a bug you want to report? Or perhaps an idea for a feature? We'd be grateful for your contributions.

* Open an issue if you have a bug to report, or a pull request with your changes.

## Getting and Building Holograms

To get a copy of the Holograms source, ensure you have Git installed, and run the following commands from a command prompt
or terminal:

1. `git clone git@bitbucket.org:shadownode/holograms.git`
2. `cd holograms`

To build Holograms, navigate to the source directory and run either:

* `./gradlew build` on UNIX and UNIX like systems (including OS X and Linux)
* `gradlew build` on Windows systems

You will find the compiled JAR which will be named like `Holograms-x.x.x-SNAPSHOT.jar` in `builds/libs/`.

[Source]: https://bitbucket.org/shadownode/holograms
[Issues]: https://bitbucket.org/shadownode/holograms/issues
[Downloads]: https://bitbucket.org/shadownode/holograms/downloads
[Website]: https://www.shadownode.ca
