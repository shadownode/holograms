package ca.shadownode.holograms.sponge;

import ca.shadownode.holograms.sponge.config.Hologram;
import ca.shadownode.holograms.sponge.utils.Utils;
import com.flowpowered.math.vector.Vector3d;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Optional;

import com.google.common.reflect.TypeToken;
import com.google.gson.*;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.NamedCause;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class HologramManager {

    private final HologramsPlugin plugin;
    public Vector3d offset = new Vector3d(0, 0.3, 0);

    public HologramManager(HologramsPlugin plugin) {
        this.plugin = plugin;
    }

    public void removeArmorstands() {
        Sponge.getServer().getWorlds().forEach((bit) -> {
            for (Entity ent : bit.getEntities()) {
                if (ent instanceof ArmorStand) {
                    ArmorStand as = (ArmorStand) ent;
                    ent.remove();
                }
            }
        });
    }

    public boolean createHologram(Hologram hologram) {

        Optional<World> world = Sponge.getServer().getWorld(hologram.worldName);

        if (!world.isPresent()) {
            plugin.getLogger().info("Failed to create hologram world name not valid.");
            return false;
        }

        Location<World> location = world.get().getLocation(hologram.x, hologram.y, hologram.z).add(offset);

        if(plugin.getConfig().debug) {
            plugin.getLogger().info("Creating Hologram at: " + location.toString());
        }

        for(String line : hologram.lines) {
            location = location.sub(offset);
            String finalLine = line;
            if (line.contains("%VERSION%")) {
                finalLine = line.replace("%VERSION%", getVersionFromLink(hologram.versionLink));
            }
            createArmorStand(location, finalLine);
        }

        return true;
    }

    public boolean createArmorStand(Location<World> location, String text) {

        ArmorStand as = null;

        location.getExtent().loadChunk(location.getChunkPosition(), false);

        if (location.getExtent().getChunk(location.getChunkPosition()).isPresent()) {
            if (location.getExtent().getChunk(location.getChunkPosition()).get().isLoaded()) {
                if (as == null || !as.isLoaded()) {
                    as = (ArmorStand) location.getExtent().createEntity(EntityTypes.ARMOR_STAND, location.getPosition().add(offset));
                    as.offer(Keys.HAS_GRAVITY, false);
                    as.offer(Keys.INVISIBLE, true);
                    as.offer(Keys.ARMOR_STAND_MARKER, true);
                    as.offer(Keys.CUSTOM_NAME_VISIBLE, true);
                    as.offer(Keys.DISPLAY_NAME, Utils.parseMessage(text));
                         
                    boolean worked = location.getExtent().spawnEntity(as, Cause.of(NamedCause.of("PluginContainer", plugin.pluginContainer)));
                    if (!worked) {
                        if (plugin.getConfig().debug) {
                            plugin.getLogger().info("Failed creating ArmorStand at: " + location.toString());
                        }
                        return false;
                    }
                    if (plugin.getConfig().debug) {
                        plugin.getLogger().info("Created ArmorStand at: " + location.toString());
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private String getVersionFromLink(String link) {
        String version = "";
        try {
            URL url = new URL(link);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type", "application/json");
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
                JsonParser parser = new JsonParser();
                JsonObject object = parser.parse(reader).getAsJsonObject();
                version = object.get("pack_version").getAsString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return version;
    }

}
