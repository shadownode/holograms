package ca.shadownode.holograms.sponge.commands;

import ca.shadownode.holograms.sponge.HologramsPlugin;
import java.io.IOException;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.text.Text;

public class ReloadCommand implements CommandExecutor {
    
    private final HologramsPlugin plugin;

    public ReloadCommand(HologramsPlugin plugin) {
        this.plugin = plugin;
    }
    
    public void register() {
        CommandSpec commandSpec = CommandSpec.builder()
                .permission("holograms.reload")
                .description(Text.of("Holograms reload command."))
                .executor(this)
                .build();

        Sponge.getCommandManager().register(plugin, commandSpec, "holoreload");
    }
    
    @Override
    public CommandResult execute(CommandSource commandSource, CommandContext commandContext) throws CommandException {
        plugin.getConfig().loadConfig();
        plugin.getManager().removeArmorstands();
        plugin.getConfig().holograms.forEach((hologram) -> {
            plugin.getManager().createHologram(hologram);
        });
        commandSource.sendMessage(Text.of("Configuration reloaded!"));
        return CommandResult.success();
    }
}
