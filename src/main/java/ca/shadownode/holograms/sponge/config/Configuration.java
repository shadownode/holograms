package ca.shadownode.holograms.sponge.config;

import ca.shadownode.holograms.sponge.HologramsPlugin;
import com.google.common.reflect.TypeToken;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;

public final class Configuration {

    private final HologramsPlugin plugin;

    private ConfigurationLoader loader;
    private ConfigurationNode config;

    public boolean debug;
    public boolean update;
    public Integer updateTime;

    public List<Hologram> holograms = new ArrayList<>();

    public Configuration(HologramsPlugin plugin) {
        this.plugin = plugin;
        loadConfig();
    }

    public void loadConfig() {
        try {
            loader = HoconConfigurationLoader.builder().setFile(plugin.defaultConfig).build();
            saveDefaultConfig();
            config = loader.load();
            debug = config.getNode("Holograms", "Debug").getBoolean(false);
            update = config.getNode("Holograms", "Update").getBoolean(false);
            updateTime = config.getNode("Holograms", "UpdateTime").getInt(30);
            holograms = config.getNode("Holograms", "List").getList(TypeToken.of(Hologram.class));

        } catch (IOException | ObjectMappingException ex) {
            plugin.getLogger().error("The default configuration could not be loaded!", ex);
        }
    }

    public void saveDefaultConfig() {
        if (!plugin.defaultConfig.exists()) {
            try {
                plugin.defaultConfig.createNewFile();
                config = loader.load();
                config.getNode("Holograms", "Debug").setValue(false);
                config.getNode("Holograms", "Update").setValue(false);
                config.getNode("Holograms", "UpdateTime").setValue(30);
                config.getNode("Holograms", "List").setValue(new TypeToken<List<Hologram>>() {
                }, Arrays.asList(new Hologram("world", 0.0, 0.0, 0.0, Arrays.asList("Line 1", "Line 2"))));
                loader.save(config);
            } catch (IOException | ObjectMappingException ex) {
                plugin.getLogger().error("The default configuration could not be created!", ex);
            }
        }
    }

}
