package ca.shadownode.holograms.sponge.config;

import java.util.Arrays;
import java.util.List;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;

@ConfigSerializable
public class Hologram {
    
    public Hologram() {
    }
    
    public Hologram(String worldName, Double x, Double y, Double z, List<String> lines) {
        this.worldName = worldName;
        this.x = x;
        this.y = y;
        this.z = z;
        this.lines = lines;
    }
    
    @Setting("WorldName")
    public String worldName = "world";

    @Setting("VersionLink")
    public String versionLink = "https://shadownode.ca/servers/api/getServerByStatsId?stats_id=lobby";
    
    @Setting("X")
    public Double x = 0.0;
    @Setting("Y")
    public Double y = 0.0;
    @Setting("Z")
    public Double z = 0.0;
    
    @Setting("Lines")
    public List<String> lines = Arrays.asList("Line 1", "Line 2");
    
    
}
