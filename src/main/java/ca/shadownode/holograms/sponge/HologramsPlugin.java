package ca.shadownode.holograms.sponge;

import ca.shadownode.holograms.sponge.commands.ReloadCommand;
import ca.shadownode.holograms.sponge.config.Configuration;
import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;

import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;

@Plugin(id = "holograms")
public class HologramsPlugin {

    @Inject
    public PluginContainer pluginContainer;
    @Inject
    private Logger logger;
    @Inject
    @DefaultConfig(sharedRoot = false)
    public File defaultConfig;

    public Configuration config;
    public HologramManager manager;

    @Listener
    public void onStart(GameStartedServerEvent event) throws IOException {
        config = new Configuration(this);
        new ReloadCommand(this).register();
        manager = new HologramManager(this);
        getLogger().info("Holograms started!");
    }

    @Listener(order = Order.POST)
    public void onPostStart(GameStartedServerEvent event) throws IOException {
        getLogger().info("Deleting existing armor stands...");
        Sponge.getScheduler().createTaskBuilder().execute(() -> {
            manager.removeArmorstands();
            config.holograms.forEach((hologram) -> {
                manager.createHologram(hologram);
            });
        }).delayTicks(1).submit(this);
        if (config.update) {
            Sponge.getScheduler().createTaskBuilder().execute(() -> {
                manager.removeArmorstands();
                config.holograms.forEach((hologram) -> {
                    manager.createHologram(hologram);
                });
            }).interval(config.updateTime, TimeUnit.SECONDS).submit(this);
        }
    }

    @Listener
    public void onStop(GameStoppingServerEvent event) throws IOException {
        manager.removeArmorstands();
    }

    public Configuration getConfig() {
        return config;
    }

    public HologramManager getManager() {
        return manager;
    }

    public Logger getLogger() {
        return logger;
    }
}
