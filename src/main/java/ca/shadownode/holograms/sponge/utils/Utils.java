package ca.shadownode.holograms.sponge.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;

public class Utils {
    
    public static final Pattern URL_PATTERN = Pattern.compile("((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)", Pattern.CASE_INSENSITIVE);

    public static Text parseMessage(String message) {
        return parseMessage(message, new HashMap<>());
    }

    public static Text parseMessage(String message, HashMap<String, String> args) {
        for (Map.Entry<String, String> arg : args.entrySet()) {
            message = message.replace("{" + arg.getKey() + "}", arg.getValue());
        }
        Text textMessage = TextSerializers.FORMATTING_CODE.deserialize(message);
        List<String> urls = extractUrls(message);

        if (!urls.isEmpty()) {
            for (String url : urls) {
                String msgBefore = StringUtils.substringBefore(message, url);
                String msgAfter = StringUtils.substringAfter(message, url);
                if (msgBefore == null) {
                    msgBefore = "";
                } else if (msgAfter == null) {
                    msgAfter = "";
                }
                try {
                    textMessage = Text.of(
                            TextSerializers.FORMATTING_CODE.deserialize(msgBefore),
                            TextActions.openUrl(new URL(url)),
                            Text.of(TextColors.GREEN, url),
                            TextSerializers.FORMATTING_CODE.deserialize(msgAfter));
                } catch (MalformedURLException e) {
                    return Text.of(message);
                }
            }
        }
        return textMessage;
    }

    public static List<Text> parseMessageList(List<String> messages) {
        return parseMessageList(messages, new HashMap<>());
    }

    public static List<Text> parseMessageList(List<String> messages, HashMap<String, String> args) {
        List<Text> texts = new ArrayList<>();
        messages.forEach(s -> texts.add(parseMessage(s, args)));
        return texts;
    }

    public static List<String> extractUrls(String text) {
        List<String> containedUrls = new ArrayList<>();
        Matcher urlMatcher;
        try {
            urlMatcher = URL_PATTERN.matcher(text);
            while (urlMatcher.find()) {
                containedUrls.add(text.substring(urlMatcher.start(0), urlMatcher.end(0)));
            }
            return containedUrls;
        } catch (Exception ignored) {
            return containedUrls;
        }
    }

}
